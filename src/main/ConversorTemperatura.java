package main;

public class ConversorTemperatura {
	
	public double celsiusParaFahrenheit (double valorCelsius){
        return 9*valorCelsius/5+32;
    }    
    public double FahrenheitParaCelsius (double valorFahrenheit){
        return 5*(valorFahrenheit - 32)/9;
    } 
    public double celsiusParaKelvin (double valorCelsius){
        return valorCelsius+273.0;
    }    
    public double FahrenheitParaKelvin (double valorFahrenheit){
        return  (valorFahrenheit-32)*5/9+273;
    }
    public double KelvinParaCelsius (double valorKelvin){
        return valorKelvin-273.0;
    }    
    public double KelvinParaFahrenheit (double valorKelvin){
    	return ((valorKelvin - 273.0)*9/5)+32;
    } 
    

}
