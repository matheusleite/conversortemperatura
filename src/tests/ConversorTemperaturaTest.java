package tests;

import main.ConversorTemperatura;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ConversorTemperaturaTest {

	private ConversorTemperatura meuConversor;
	
	@Before
	public void setUp() throws Exception{
		meuConversor = new ConversorTemperatura();
	}
	
	@Test
	public void testCelsiusParaFahrenheit(){
		assertEquals(104.0, meuConversor.celsiusParaFahrenheit(40.0),0.01);
	}
	@Test
	public void testCelsiusParaKelvin(){
		assertEquals(313.0, meuConversor.celsiusParaKelvin(40.0),0.01);
	}
	@Test
	public void testFahrenheitParaCelsius(){
		assertEquals(37.78, meuConversor.FahrenheitParaCelsius(100),0.01);
	}
	@Test
	public void testFahrenheitParaKelvin(){
		assertEquals(310.77, meuConversor.FahrenheitParaKelvin(100),0.01);
	}
	@Test
	public void testKelvinParaCelsius(){
		assertEquals(27, meuConversor.KelvinParaCelsius(300),0.01);
	}
	@Test
	public void testKelvinParaFahrenheit(){
		assertEquals(80.6, meuConversor.KelvinParaFahrenheit(300),0.01);
	}

}
